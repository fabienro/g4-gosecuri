package com.g4gosecuri.firebase;

import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.Test;

import com.g4gosecuri.firebase.model.User;

import junit.framework.Assert;

public class UserTest {
	@Test
	public void should_update_last_name_when_new_last_name() {
		// given
		User user = new User("john.doe@example.fr", "super password", "Doe", "johnny", "5");
		
		// when
		user.setLastname("Doudou");
		
		// then
		Assert.assertEquals(user.getLastname(), ("Doudou"));
	}
}

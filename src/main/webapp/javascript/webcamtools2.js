$(document).ready(function() {
	//Configure the camera settings
	function ShowCam() {
		Webcam.set({
			width : 300,
			height : 240,
			image_format : 'jpeg',
			jpeg_quality : 100
		});
		Webcam.attach('#my_camera');
	}
	
	//ask for camera and set settings
	ShowCam();

	//action when button btnSnapit is clicked
	$('#btnSnapit').click(function() {

		Webcam.snap(function(data_uri) {
			//fill the img tag src with received data
			$('#base64image')[0].src = data_uri;
		});
	});

	//action when button btnSave is clicked
	$('#btnSave').click(function() {
		
		// get the image
		var file = $('#base64image')[0].src;
		var formdata = new FormData();
		formdata.append("base64image", file);
		
		// Ajouter le pseudo de l'utilisateur et le numéro de la photo : 
		var pseudo = document.getElementById("pseudo").value;
		var tmpNumber = document.getElementById("number").value;
		var tmpPseudo = pseudo.concat("_",tmpNumber);
		
		// Vérification de la bonne réception des paramètres
		console.log("--------");
		console.log(file);
		console.log(pseudo);
		console.log(tmpPseudo);
		console.log(tmpNumber);

		
		//Partage de la photo à la Servlet 
		$.ajax({
			  type: "POST",
			  contentType: 'application/x-www-form-urlencoded',
			  url: "/g4-gosecuri/CameraPictureServlet",
			  data: {
					"img" : file,
					"pseudo" : tmpPseudo,
					"number" : tmpNumber,
				},
			  success: uploadcomplete(event),
			})
	});

	function uploadcomplete(event) {
		// say when image is saved
		alert("IMAGE SAUVEGARDEE");
	}
	
});

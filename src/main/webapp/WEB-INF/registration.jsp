<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>INSCRIPTION</title>
<jsp:include page="header.jsp" />
</head>
<body>
<!-- Page de base -->
<div class="registration-page">
<div class="form">
<!-- Saut de ligne -->
<br>
<br>
<!-- Formulaire d'enregistrement -->
<div id="registration">
<form method="post" action="registration" >
<b>ENREGISTRER UN NOUVEAU COLLABORATEUR</b>
<br>
<br>
<input id="email" name="email" type="text" class="inputChamp" placeholder="Votre email *" /><br />
<input id="password" name="password" type="text" class="inputChamp" placeholder="Votre mot de passe *" /><br />
<input id="lastname" name="lastname" type="text" class="inputChamp" placeholder="Votre nom de famille *" /><br />
<input id="pseudo" name="pseudo" type="text" class="inputChamp" placeholder="Votre pseudo *" /><br />
<fieldset>
<label class="container">
  <p>
    <input type="radio" name="levelUser" id="levelUser" value="user" checked>
    <label for="utiisateur">Utilisateur</label>
  </p>
  <p>
    <input type="radio" name="levelUser" id="levelUser" value="admin" >
    <label for="administrateur">Administrateur</label>
  </p>
</label>
</fieldset>
<br/>
<!-- Bouton pour soumettre son formulaire -->
<button type="submit" value="Je 'm'inscris" class="submitBtn" />INSCRIPTION</button>
</form>
<br>
<button onClick="javascript:document.location.href='/g4-gosecuri/AdminSpaceServlet'">RETOUR AUX OPTIONS</button>
</div>
</div>
</div>
</body>
<footer>
<jsp:include page="footer.jsp" />
</footer>
</html>



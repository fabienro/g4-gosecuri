<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page import="com.g4gosecuri.firebase.model.Material"%> 
<%@page import="java.util.List"%> 
<!DOCTYPE html>
<html>
<head>
<title>RETRAIT DE MATERIEL</title>
<jsp:include page="header.jsp" />
</head>
<body>
	<div class="homepage-page">
		<div class="form">
		<div><b>OBJETS DISPONIBLES</b></div>
   	<% List<Material> materials = (List<Material>) request.getAttribute("materials"); %>
   	<form id="inventory" name="inventory" method="POST">
   		<ul style="list-style: none;">
   		<% if (materials != null) {%>
			<% for(Material material: materials) { %>
			<% if (material.getQuantity() == 0) { %>
		        <li> 
					<input type="checkbox" id="<%= material.getName() %>" name="<%= material.getName() %>" disabled>
		  			<label for="<%= material.getName() %>"><%= material.getName() %> (<%= material.getQuantity() %>)</label>
		  		</li>
		  	<% } else { %>
				<li> 
					<input type="checkbox" id="<%= material.getName() %>" name="<%= material.getName() %>">
		  			<label for="<%= material.getName() %>"><%= material.getName() %> (<%= material.getQuantity() %>)</label>
		  		</li>
	     	<% }
			}%>
		<% }%>
     	</ul>
     <button type="submit">Valider</button>
  	</form>
</div>
</div>
</body>
<footer>
	<!-- Insertion de la JSP Footer-->
	<jsp:include page="footer.jsp" />
</footer>
</html>
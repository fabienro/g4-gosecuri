<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<!-- Insertion de la feuille de style-->
<link rel="stylesheet" href="style/style.css" />
<!-- Script pour bouton-->
<script type="text/javascript" src="javascript/javascript.js"></script>
<!-- Insertion de la favicon -->
<link rel="icon" type="image/x-icon" href="/favicon.ico" /><link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
</html>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>MATERIEL</title>
<jsp:include page="header.jsp" />
</head>
<body>
<!-- Page de base -->
<div class="restocking-page">
<div class="form">
<!-- Saut de ligne -->
<br>
<br>
<!-- Formulaire d'enregistrement -->
<div id="restocking">
<form method="post" action="restocking">
<b>ENREGISTRER UN NOUVEL OBJET</b>
<br>
<br>
<input id="name" name="name" type="text" class="inputChamp" placeholder="Nom de l'objet *" /><br />
<input id="quantity" name="quantity" type="text" class="inputChamp" placeholder="Renseigner la quantité disponible *" /><br />
<br/>
<!-- Bouton pour soumettre son formulaire -->
<button type="submit" value="Je renseigne l'objet" class="submitBtn" />AJOUT DE L'OBJET</button>
</form>
<br>
<button onClick="javascript:document.location.href='/g4-gosecuri/AdminSpaceServlet'">RETOUR AUX OPTIONS</button>
</div>
</div>
</div>
</body>
<footer>
<jsp:include page="footer.jsp" />
</footer>
</html>



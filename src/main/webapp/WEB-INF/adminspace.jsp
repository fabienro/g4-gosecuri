<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.g4gosecuri.firebase.model.Picture"%>
<!DOCTYPE html>
<html>
<style>
.logo {
      width: 300px;
      height: 240px;
      background: url(data:image/png;base64,copy-paste-base64-data-here) no-repeat;
      margin-left:auto;
	  margin-right:auto;
}
</style>
<head>
<title>ESPACE ADMINISTRATEUR</title>
<jsp:include page="header.jsp" />
</head>
<body>
	<!-- Page de base -->
	<div class="homepage-page">
		<div class="form">
			<br>
			<br>
			<div class="logo"><IMG SRC="<%= request.getAttribute("pictureUserDecoded") %>"/></div>
			<br>
		   	<br>
			<b>ESPACE ADMINISTRATEUR</b>
		   	<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/RegistrationServlet'">AJOUTER UN UTILISATEUR</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/RestockingServlet'">AJOUTER DU MATERIEL</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/CameraPictureServlet'">AJOUTER UNE PHOTO</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/GestionMaterielServlet'">EMPRUNTER DU MATERIEL</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/GestionRetourMaterielServlet'">RENDRE DU MATERIEL</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/LogoutServlet'">IDENTIFICATION</button>
		</div>
	</div>
</body>
<footer>
	<!-- Insertion de la JSP Footer-->
	<jsp:include page="footer.jsp" />
</footer>
</html>
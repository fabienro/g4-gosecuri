<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page import="com.g4gosecuri.firebase.model.Inventory"%> 
<%@page import="com.g4gosecuri.firebase.model.Material"%> 
<%@page import="java.util.List"%> 
<!DOCTYPE html>
<html>
<head>
<title>RETOUR DE MATERIEL</title>
<jsp:include page="header.jsp" />
</head>
<body>
	<div class="homepage-page">
	<div class="form">
	<div><b>OBJETS DANS VOTRE INVENTAIRE</b></div>
	<br>
   	<% List<String> inventory = (List<String>) request.getAttribute("items"); %>
   	<form id="inventory" name="inventory" method="POST">
   		<ul style="list-style: none;">
   		<% if (inventory != null) {%>
			<% for(String item: inventory) { %>
		        <li> 
					<input type="checkbox" id="<%= item %>" name="<%= item %>">
		  			<label for="<%= item %>"><%= item %></label>
		  		</li>
			<%} %>
		<% }%>
     	</ul>
     <button type="submit">Valider</button>
  	</form>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/GestionMaterielServlet'">EMPRUNTER DU MATERIEL</button>
			<br>
			<br>
			<button onClick="javascript:document.location.href='/g4-gosecuri/LogoutServlet'">IDENTIFICATION</button>
</div>
</div>
</body>
<footer>
	<!-- Insertion de la JSP Footer-->
	<jsp:include page="footer.jsp" />
</footer>
</html>
<%@ page pageEncoding="UTF-8" %>
<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Titre de la page-->
<title>PRENDRE UNE PHOTO</title>
<link rel="stylesheet" type="text/css" href="./style/style.css">
	<script type="text/javascript" src="./javascript/javascript.js"></script>
	<script type="text/javascript" src="./javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="./javascript/webcam.js"></script>
	<script type="text/javascript" src="./javascript/webcamtools2.js"></script>
<!-- Insertion de la JSP Header-->
<jsp:include page="header.jsp" />
</head>

<body>

<!-- Début du bloc page -->
<div class="login-page">
  <div class="form">
<div>
<form>
<input id="number" name="number" type="text" class="inputChamp" placeholder="Entrer le numéro de la photo *" /><br />
<input id="pseudo" name="pseudo" type="text" class="inputChamp" placeholder="Entrer le Pseudo de l'utilisateur *" /><br />
</form>
</div>
	<div class="container" id="Cam">
		<b></b>
		<div id="my_camera"></div>
		<form>
		<button type="button" id="btnSnapit" value="PRENDRE LA PHOTO">PRENDRE PHOTO</button>
		<br>
		<br>
		</form>
		
		</div>

		<!-- SAUVEGARDER L'image -->
	<div class="container" id="Prev">
		<b></b>
		<div id="results">
			<img id="base64image" src="" />
			<button id="btnSave" >SAUVEGARDER</button>
		</div>
	</div>
<br>
<br>
<button onClick="javascript:document.location.href='/g4-gosecuri/AdminSpaceServlet'">RETOUR AUX OPTIONS</button>
  </div>
</div>
	
</body>
<footer>
<h4><center><p>&copy; G4 - 2019/20 - THE NUUM FACTORY</p></center></h4>
</footer>
</html>

</head>
<body>

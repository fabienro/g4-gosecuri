<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>S'AUTHENTIFIER</title>
<jsp:include page="header.jsp" />
</head>
<body>
<!-- Page de base -->
<div class="login-page">
<div class="form">
<!-- Saut de ligne -->
<br>
<br>
<b>AUTHENTIFICATION</b>
<br>
<br>
<!-- Module d'Authentification -->
<div id="login">
<form method="post" action="login" >
<input id="pseudoUser" name="pseudoUser" type="text" class="inputChamp" placeholder="Votre pseudo *" /><br />
<input id="passwordUser" name="passwordUser" type="text" class="inputChamp" placeholder="Votre mot de passe *" /><br />
<br>
<div>
<fieldset>
<label class="container">
  <p>
    <input type="radio" name="levelUser" id="levelUser" value="admin" >
    <label for="administrateur">ADMINISTRATEUR</label>
  </p>
</label>
</fieldset>
</div>
<br>
<button type="submit" value="Je m'authentifie" class="submitBtn" />CONFIRMATION</button>
</form>
</div>
</div>
</div>
</body>
<footer>
<jsp:include page="footer.jsp" />
</footer>
</html>



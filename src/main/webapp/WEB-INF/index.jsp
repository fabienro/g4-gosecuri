<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>ACCUEIL</title>
<jsp:include page="header.jsp" />
</head>
<body>
	<!-- Page de base -->
	<div class="homepage-page">
		<div class="form">
			<!-- Insertion du logo dans le header -->
			<img src="img/go_securi.png" width=141,2 height=115,06 ALT="GO Securi">
			<!-- Saut de ligne -->
			<br>
			<br>	
			<br>
			<br>	
			<!-- Bouton  pour s'identifier-->
			<button onClick="javascript:document.location.href='/g4-gosecuri/LoginServlet'">S'AUTHENTIFIER</button>
			<!-- Saut de ligne -->
			<br>
			<br>
		</div>
	</div>
</body>
<footer>
	<!-- Insertion de la JSP Footer-->
	<jsp:include page="footer.jsp" />
</footer>
</html>

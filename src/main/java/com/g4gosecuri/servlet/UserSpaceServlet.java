package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet est une Servlet de redirection vers l'espace utilisateur.
 * 
 * */

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.controller.UsersAuthentificationDao;
import com.g4gosecuri.firebase.model.Picture;


@WebServlet(name="/UserSpaceServlet", urlPatterns= {"/userspace"})
public class UserSpaceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String pseudo;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSpaceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		HttpSession session = request.getSession();
		session.getAttribute("pseudoUser");
		session.getAttribute("levelUser");
		pseudo = (String) session.getAttribute("pseudoUser");
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		UsersAndMaterialDao uDao = getUserDao(contextBiString);
		UsersAuthentificationDao authDao= new UsersAuthentificationDao(uDao);
		Picture pictureUser = uDao.getPicture(pseudo);
		String pictureUserDecoded = pictureUser.getImg();
		request.setAttribute("pictureUserDecoded", pictureUserDecoded);
		PrintWriter out = response.getWriter();
		this.getServletContext().getRequestDispatcher("/WEB-INF/userspace.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		Object conn = context.getAttribute("ConnBase");
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) conn;
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}

}
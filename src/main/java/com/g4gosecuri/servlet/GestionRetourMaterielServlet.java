package com.g4gosecuri.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.controller.UsersAuthentificationDao;
import com.g4gosecuri.firebase.model.Inventory;
import com.g4gosecuri.firebase.model.Material;

/**
 * Cette Servlet gère les retours matériel
 */
@WebServlet(name="/GestionRetourMaterielServlet", urlPatterns= {"/restockingMovOut"})
public class GestionRetourMaterielServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String pseudo;
	private String level;
	private List<String> inventory;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionRetourMaterielServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.getAttribute("pseudoUser");
		session.getAttribute("levelUser");
		pseudo = (String) session.getAttribute("pseudoUser");
		System.out.println("Pseudo de l'utilisateur en session : " + pseudo);
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		UsersAndMaterialDao uDao = getUserDao(contextBiString);	
		Inventory inventory = uDao.getInventory(pseudo);
		List<String> items = inventory.getItems();
		request.setAttribute("items", items);
		this.getServletContext().getRequestDispatcher("/WEB-INF/restockingMovOut.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Enumeration<String> a = request.getParameterNames();
		List<String> parameters = Collections.list(a);
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		UsersAndMaterialDao uDao = getUserDao(contextBiString);
		UsersAuthentificationDao authDao= new UsersAuthentificationDao(uDao);
		
		
		List<Material> mForPost = uDao.getAllMaterials();
		List<Material> mToUpdateForPost = new ArrayList<Material>();
		for (String parameter: parameters){			
			for (Material material: mForPost) {
				
				String name = material.getName();
				System.out.println("Material name " + name);
				System.out.println("Parameter " + parameter);

				if(name.contentEquals(parameter)) {
					mToUpdateForPost.add(material);
				}
			}
		}
		for (Material material: mToUpdateForPost) {
			String name = material.getName();
			int oldQuantity = material.getQuantity();
			Material updatedMaterial = new Material(name, oldQuantity + 1);
			uDao.addMaterial(updatedMaterial);
		}
		
		Inventory inventoryForPost = uDao.getInventory(pseudo);
		List<String> itemsForPost = inventoryForPost.getItems();
		List<String> newItemsForPost = new ArrayList<String>();
		for(Material material: mToUpdateForPost) {
			String name = material.getName();
			
			for (String item: itemsForPost) {
				if(!item.equals(name)) {
					newItemsForPost.add(item);
				}
			}
		}
		inventoryForPost.setItems(newItemsForPost); 
		uDao.addInventory(inventoryForPost);
		this.getServletContext().getRequestDispatcher("/WEB-INF/restockingMovOut.jsp").forward(request, response);		

	}
	
	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		Object conn = context.getAttribute("ConnBase");
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) conn;
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}

}

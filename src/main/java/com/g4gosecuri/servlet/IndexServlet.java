package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet est une Servlet de redirection vers l'index de l'application.
 * 
 * */

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;

@WebServlet(name="/IndexServlet", urlPatterns= {"", "/index"})
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String session;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
		System.out.println("Vérification de session, null si aucune : " + session);
		if (request.getSession(false) == null )
		{
		   System.out.println("Aucune session en cours");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet permet de s'authentifier, que l'on soit utilisateur ou administrateur du local.
 * 
 * */

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAuthentificationDao;
import com.g4gosecuri.firebase.model.Picture;
import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;

@WebServlet(name="/LoginServlet", urlPatterns= {"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		String pseudo=request.getParameter("pseudoUser");
		System.out.println("Login : " + pseudo);
		String password=request.getParameter("passwordUser");
		System.out.println("mot de passe : " + password);
		String level=request.getParameter("levelUser");
		System.out.println("null si pas admin : " + level);
		System.out.println("Récupération des informations utilisateur réussie");
		session.setAttribute("pseudoUser", pseudo);
		session.setAttribute("levelUser", level);
		/** Instanciation de la DAO */
		UsersAndMaterialDao uDao = getUserDao(contextBiString);
		UsersAuthentificationDao authDao= new UsersAuthentificationDao(uDao);
		Picture pictureUser = uDao.getPicture(pseudo);
		String pictureUserDecoded = pictureUser.getImg();
		request.setAttribute("pictureUserDecoded", pictureUserDecoded);
		PrintWriter out = response.getWriter();

		/** Création d'un dispatcher pour rediriger l'utilisateur selon ses actions */
		RequestDispatcher dispatcher ;
		if(authDao.authentifierMdp(pseudo, password)){
			System.out.println("Utilisateur authentifié, redirection vers son espace personnel");
			if (authDao.authentifierLevel(pseudo, level)) {
				/** L'authentification réussie, l'utilisateur accède à son espace */
				dispatcher=request.getRequestDispatcher("/WEB-INF/adminspace.jsp");
			System.out.println("Administrateur connecté, redirection vers son espace personnel");
				dispatcher.forward(request, response) ;				
			}
			else {
				/** L'authentification réussie, l'utilisateur accède à son espace */
				dispatcher=request.getRequestDispatcher("/WEB-INF/userspace.jsp");
			System.out.println("Utilisateur connecté, redirection vers son espace personnel");
				dispatcher.forward(request, response) ;
			}
		}
		else {	
			/** L'authentification échoue, l'utilisateur est invité à se connecter à nouveau */
			System.out.println("Utilisateur non authentifié, redirection vers la JSP index disponible");
			dispatcher=request.getRequestDispatcher("/WEB-INF/index.jsp");
			dispatcher.forward(request, response) ;	
		}	
	}
	
	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) context.getAttribute("ConnBase");
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}

	
	
}
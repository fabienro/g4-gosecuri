package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet permet d'ajouter du matériel dans la base de données Firebase.
 * 
 * */

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.model.Material;

@WebServlet(name="/RestockingServlet", urlPatterns= {"/restocking"})
public class RestockingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestockingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/restocking.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/** Récupération des informations tapées par l'utilisateur */
		String erreur = "";
		HttpSession session = request.getSession();
		
		String name = request.getParameter("name");
		if (name.isEmpty()) {
			erreur += "Veuillez renseigner le nom de l'objet<br>";
		}
		String quantityToString = request.getParameter("quantity");
		int quantity = Integer.parseInt(quantityToString);		
		System.out.println("Quantité renseignée : " + quantity);
		if (quantity<=0) {
		erreur += "Veuillez renseigner le mot de passe<br>";
		}
		
		Material m = new Material(name, quantity);
		
		if (erreur.isEmpty()) {
			ServletContext context = getServletContext();
			String contextBiString = context.getRealPath("/WEB-INF");
			UsersAndMaterialDao uDao = getUserDao(contextBiString);			
			uDao.addMaterial(m);
			session.setAttribute("material", m);			
			response.sendRedirect("AdminSpaceServlet");
		}
		else {
			
			request.setAttribute("erreur", erreur);
			request.setAttribute("material", m);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/adminspace.jsp").forward(request, response);		
		}
		
	}
	
	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) context.getAttribute("ConnBase");
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}
	
}
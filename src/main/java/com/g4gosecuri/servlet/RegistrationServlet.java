package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet permet d'enregistrer un nouvel utilisateur dans la base de données Firebase.
 * 
 * */

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.controller.UsersAuthentificationDao;
import com.g4gosecuri.firebase.model.Inventory;
import com.g4gosecuri.firebase.model.User;

@WebServlet(name="/RegistrationServlet", urlPatterns= {"/registration"})
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/registration.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/** Récupération des informations tapées par l'utilisateur */
		String erreur = "";
		HttpSession session = request.getSession();
		
		String email = request.getParameter("email");
		if (email.isEmpty()) {
			erreur += "Veuillez renseigner l'email<br>";
		}
		String password = request.getParameter("password");
		if (password.isEmpty()) {
			erreur += "Veuillez renseigner le mot de passe<br>";			
		}
		String lastname = request.getParameter("lastname");
		if (lastname.isEmpty()) {
			erreur += "Veuillez renseigner le nom de famille<br>";						
		}
		String pseudo = request.getParameter("pseudo");
		if (pseudo.isEmpty()) {
			erreur += "Veuillez renseigner le pseudo<br>";									
		}
		String level = request.getParameter("levelUser");
		if (level.isEmpty()) {
			erreur += "Veuillez renseigner le level<br>";									
		}
		
		User u = new User(email, password, lastname, pseudo, level);
		
		if (erreur.isEmpty()) {
			ServletContext context = getServletContext();
			String contextBiString = context.getRealPath("/WEB-INF");
			//UsersAndMaterialDao dao = new UsersAndMaterialDao(contextBiString);
			UsersAndMaterialDao uDao = getUserDao(contextBiString);			
			//dao.addUser(u);
			uDao.addUser(u);
			session.setAttribute("user", u);			
			response.sendRedirect("LoginServlet");
		}
		else {
			
			request.setAttribute("erreur", erreur);
			request.setAttribute("user", u);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);		
		}
		
	}
	
	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) context.getAttribute("ConnBase");
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}
	
}
package com.g4gosecuri.servlet;

/** 
 * 
 * Cette Servlet doit permettre de récupérer une photo d'utilisateur et la faire suivre à la base de données.
 * 
 * */

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.model.Picture;

@WebServlet(name="CameraPictureServlet", urlPatterns= {"/camerapicture"})
public class CameraPictureServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public CameraPictureServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		this.getServletContext().getRequestDispatcher("/WEB-INF/camerapicture.jsp").forward(request, response);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/** Récupération des informations de la photo pour un stockage en variable */
		String pictureImg = request.getParameter("img");
		String picturePseudo = request.getParameter("pseudo");
		String pictureNumber = request.getParameter("number");
		Picture p = new Picture(pictureImg,picturePseudo,pictureNumber);
		System.out.println(pictureImg);
		System.out.println(picturePseudo);
		System.out.println(pictureNumber);
		System.out.println("Instanciation de la photo");
		
		//AJOUTER UNE METHODE NOMMER PHOTO
		// Prendre en parametre uDao et/ou nom d'utilisateur en String + Numéro
		
		System.out.println("Tentative d'envoi de la photo à Firebase");
		ServletContext context = getServletContext();
		//Donne le chemin complet
		String contextBiString = context.getRealPath("/WEB-INF");
		
		/** Instanciation de la DAO	*/ 
		UsersAndMaterialDao uDao = getUserDao(contextBiString);
		uDao.addPicture(p);
		System.out.println("Envoi réussi");

		
	}
	
	
	/** Création d'une méthode pour réaliser une connexion unique */
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) context.getAttribute("ConnBase");
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}
	
}
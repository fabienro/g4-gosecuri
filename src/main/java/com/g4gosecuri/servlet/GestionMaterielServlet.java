package com.g4gosecuri.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.g4gosecuri.firebase.controller.UsersAndMaterialDao;
import com.g4gosecuri.firebase.controller.UsersAuthentificationDao;
import com.g4gosecuri.firebase.model.Inventory;
import com.g4gosecuri.firebase.model.Material;
import com.g4gosecuri.firebase.model.User;

/**
 * Cette Servlet gère les emprunts matériel
 */

@WebServlet(name="/GestionMaterielServlet", urlPatterns= {"/restockingMovIn"})
public class GestionMaterielServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String pseudo;
	private String level;
	private List<String> inventory;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.getAttribute("pseudoUser");
		session.getAttribute("levelUser");
		pseudo = (String) session.getAttribute("pseudoUser");
		System.out.println("Pseudo de l'utilisateur en session : " + pseudo);
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		UsersAndMaterialDao uDao = getUserDao(contextBiString);		
		List<Material> materials = uDao.getAllMaterials();
		request.setAttribute("materials", materials);
		PrintWriter out = response.getWriter();
		this.getServletContext().getRequestDispatcher("/WEB-INF/restockingMovIn.jsp").forward(request, response);
	}	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Enumeration<String> a = request.getParameterNames();
		List<String> parameters = Collections.list(a);
		ServletContext context = getServletContext();
		String contextBiString = context.getRealPath("/WEB-INF");
		UsersAndMaterialDao uDao = getUserDao(contextBiString);
		UsersAuthentificationDao authDao= new UsersAuthentificationDao(uDao);
		List<Material> materials = uDao.getAllMaterials();

		List<Material> materialsToUpdate = new ArrayList<Material>();
		
		for (String parameter: parameters){			
			for (Material material: materials) {
				System.out.println(parameter);
				if(material.getName().contains(parameter)) {
					materialsToUpdate.add(material);
				}
			}
		}
		
		for (int i = 0; i < materialsToUpdate.size(); i++) {
			Material oldMaterial = materialsToUpdate.get(i);
			if (oldMaterial.getQuantity() > 0) {
				String name = oldMaterial.getName();
				int newQuantity = oldMaterial.getQuantity() - 1;
				
				Material newMaterial = new Material(name, newQuantity);
				materialsToUpdate.set(i, newMaterial);
			}
		}
		
		for (Material material: materialsToUpdate) {
			uDao.addMaterial(material);
		}
		
		Inventory inventoryUser = new Inventory (parameters, pseudo);
		uDao = getUserDao(contextBiString);
		uDao.addInventory(inventoryUser);
		session.setAttribute("Matériel pour l'utilisteur", inventoryUser);			
		RequestDispatcher dispatcher ;
		if (authDao.authentifierLevel(pseudo, level)) {
			dispatcher=request.getRequestDispatcher("/WEB-INF/adminspace.jsp");
			dispatcher.forward(request, response) ;				
		}
		else {
			dispatcher=request.getRequestDispatcher("/WEB-INF/userspace.jsp");
			dispatcher.forward(request, response) ;
		}

	}

	/**	 Création d'une méthode pour réaliser une connexion unique	*/
	public UsersAndMaterialDao getUserDao(String contextBiString)
	{
		ServletContext context = getServletContext();
		Object conn = context.getAttribute("ConnBase");
		UsersAndMaterialDao uDao = (UsersAndMaterialDao) conn;
		if(uDao==null)
		{
			uDao = new UsersAndMaterialDao(contextBiString);
			context.setAttribute("ConnBase", uDao);
		}
		
		return uDao;
	}
	
}
package com.g4gosecuri.firebase.model;

/** 
 * 
 * Cette classe est le model de matériel, elle permet de gérer la référence identique dans les collections de la Firebase
 * 
 */

public class Material {

	private String name;
	private int quantity;
	
	public Material() {
		
	}
	
	public Material(String name, int quantity) {
		super();
		this.name = name;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}

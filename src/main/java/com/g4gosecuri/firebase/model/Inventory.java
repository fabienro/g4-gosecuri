package com.g4gosecuri.firebase.model;

import java.util.ArrayList;
import java.util.List;


	public class Inventory {
		
		private List<String> items = new ArrayList<String>();
		private String pseudo;
		
		public Inventory() {
			super();
		}

		public Inventory(List<String> items, String pseudo) {
			super();
			this.items = items;
			this.pseudo = pseudo;
		}

		public List<String> getItems() {
			return items;
		}

		public void setItems(List<String> items) {
			this.items = items;
		}

		public String getPseudo() {
			return pseudo;
		}

		public void setPseudo(String pseudo) {
			this.pseudo = pseudo;
		}
		
	}

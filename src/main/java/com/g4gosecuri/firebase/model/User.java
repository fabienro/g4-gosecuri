package com.g4gosecuri.firebase.model;

/** 
 * 
 * Cette classe est le model d'utilisateur, elle permet de gérer la référence identique dans les collections de la Firebase
 * 
 */

public class User {

	private String email;
	private String password;
	private String lastname;
	private String pseudo;
	private String level;
	
	public User() {
		super();
	}
	
	public User(String email, String password, String lastname, String pseudo, String level) {
		super();
		this.email = email;
		this.password = password;
		this.lastname = lastname;
		this.pseudo = pseudo;
		this.level = level;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "User [email=" + email + 
				", password=" + password + 
				", lastname=" + lastname + 
				", pseudo=" + pseudo +
				", level=" + level + 
				"]";
	}
}

package com.g4gosecuri.firebase.model;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public class Picture {
	
	private String img;
	private String pseudo;
	private String number;
	
	public Picture() {
		super();
	}

	
		
	public Picture(String img, String pseudo, String number) {
		super();
		this.img = img;
		this.pseudo = pseudo;
		this.number = number;
	}


	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}



	public Object[] split(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.g4gosecuri.firebase.controller;

/** 
 * 
 * Cette DAO permet de récupérer et d'envoyer des informations aux différentes collections de la base de données Firebase
 * 
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import com.g4gosecuri.firebase.model.Material;
import com.g4gosecuri.firebase.model.Picture;
import com.g4gosecuri.firebase.model.User;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.g4gosecuri.firebase.model.Inventory;

public class UsersAndMaterialDao {
	private Firestore db;
	
	public UsersAndMaterialDao(String webinf_path) {
		FileInputStream serviceAccount;
		try {
			serviceAccount = new FileInputStream(webinf_path+"/gosecuridatabase-firebase-adminsdk-b3y82-6b26ac508a.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://gosecuridatabase.firebaseio.com").build();
			FirebaseApp.initializeApp(options);				
			this.db = FirestoreClient.getFirestore();
			System.out.println("Connection to Firebase done!");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	public void addMaterial(Material m) {
		ApiFuture<WriteResult> future = db.collection("material").document(m.getName()).set(m);
		try {
			System.out.println("Updated " + m.getName() + " success at" + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public Material getMaterial(String name) {
		try {
			DocumentReference docRef = db.collection("material").document(name);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				System.out.println(document.getData());
				Material m = document.toObject(Material.class);
				return m;
			} else {
				System.out.println("no such document!");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}		
	
	
	public void addUser(User u) {
		ApiFuture<WriteResult> future = db.collection("users").document(u.getPseudo()).set(u);
		try {
			System.out.println("Updated " + u.getPseudo() + " success at" + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public User getUser(String pseudo) {
		try {
			DocumentReference docRef = db.collection("users").document(pseudo);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				System.out.println(document.getData());
				User u = document.toObject(User.class);
				return u;
			} else {
				System.out.println("no such document!");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public void addPicture(Picture p) {
		System.out.println(p.toString());
		ApiFuture<WriteResult> future = db.collection("users_pictures").document(p.getPseudo()).set(p);
		try {
			System.out.println("Updated " + p.getPseudo() + " success at" + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public Picture getPicture(String pseudo) {
		try {
			DocumentReference docRef = db.collection("users_pictures").document(pseudo);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				System.out.println(document.getData());
				Picture p = document.toObject(Picture.class);
				return p;
			} else {
				System.out.println("no such document!");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Material> getAllMaterials() {
		List<Material> materials = new ArrayList<>();
		
		try {
			ApiFuture<QuerySnapshot> future = db.collection("material").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			for (DocumentSnapshot document : documents) {
				Material material = document.toObject(Material.class);
				materials.add(material);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return materials;
	}	
	
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		
		try {
			ApiFuture<QuerySnapshot> future = db.collection("users").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			for (DocumentSnapshot document : documents) {
				User user = document.toObject(User.class);
				users.add(user);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}	
	
	public void addInventory(Inventory inv) {
		System.out.println(inv.toString());
		ApiFuture<WriteResult> future = db.collection("inventory").document(inv.getPseudo()).set(inv);
		try {
			System.out.println("Updated " + inv.getPseudo() + " success at" + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	public Inventory getInventory(String pseudo) {
		try {
			DocumentReference docRef = db.collection("inventory").document(pseudo);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				System.out.println(document.getData());
				Inventory i = document.toObject(Inventory.class);
				return i;
			} else {
				System.out.println("no such document!");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

}

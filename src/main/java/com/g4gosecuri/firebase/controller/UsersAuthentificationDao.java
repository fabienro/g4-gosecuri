package com.g4gosecuri.firebase.controller;

/** 
 * 
 * Cette DAO permet d'authentifier les utilisateurs et l'administrateur du local
 * 
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.PasswordAuthentication;
import java.sql.PseudoColumnUsage;
import java.util.concurrent.ExecutionException;

import com.g4gosecuri.firebase.model.Material;
import com.g4gosecuri.firebase.model.User;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

	public class UsersAuthentificationDao {
		/** Importation des informations de l'autre DAO */
		private UsersAndMaterialDao uDao;
		
		/** Création d'un constructeur vide */
		public UsersAuthentificationDao()
		{
			
		}
		
		/** Création d'un constructeur avec paramètres */
		public UsersAuthentificationDao(UsersAndMaterialDao uDao) {
			super();
			this.uDao = uDao;
		}
		
		/** Création d'une méthode pour comparer les mots de passe entrés et ceux de la base Firebase */
		public boolean authentifierMdp(String pseudo, String password){
			User user = uDao.getUser(pseudo);
			if (user!=null)
			{
				if (user.getPassword().equals(password))
				{
					System.out.println("password rentré"+password);
					System.out.println("password reel"+user.getPassword());
					return true;
				}
			}
			return false;
		}
		
		/** Création d'une méthode pour comparer les mots de passe entrés et ceux de la base Firebase */
		public boolean authentifierLevel(String pseudo, String level){
			User user = uDao.getUser(pseudo);
			if (user!=null)
			{
				if (user.getLevel().equals(level))
				{
					System.out.println("Level : "+level);
					return true;
				}
			}
			return false;
		}
}
		